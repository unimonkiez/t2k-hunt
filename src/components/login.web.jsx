import React, { Component } from 'react';
import PropTypes from 'prop-types';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';

export default class LoginComponent extends Component {
  static defaultProps = {
    route: undefined
  };
  static propTypes = {
    route: PropTypes.shape({}),
    hint: PropTypes.bool.isRequired,
    login: PropTypes.func.isRequired
  };
  componentWillMount() {
    this.handleSubmit = this.handleSubmit.bind(this);
    this.state = {
      isMountedAndCreatedByRouter: false
    };
  }
  componentDidMount() {
    // Chnage 'isMountedAndCreatedByRouter' from false to true
    // only when generated from router for the first time.
    // For animation effect
    if (this.props.route !== undefined) {
      setTimeout(() => this.setState({ isMountedAndCreatedByRouter: true }));
    }
  }
  handleSubmit(e) {
    e.preventDefault();

    const email = this._emailRef.input.value;
    const password = this._passwordRef.input.value;

    this.props.login(email, password);
  }
  render() {
    const { hint } = this.props;
    const hideLogin = false;

    return (
      <div style={{ position: 'fixed', left: 0, top: 0, width: '100%', height: '100%', textAlign: 'center', backgroundColor: '#dbdcf5', color: 'black' }}>
        <div style={{ position: 'relative', top: '50%', transform: 'translateY(-50%)' }}>
          <div>
            <span style={{ fontSize: '80px' }}>
              Echo hunt
            </span>
          </div>
          <div style={{ maxHeight: hideLogin ? '0' : '310px', overflow: 'hidden', transition: 'max-height 0.5s ease-in-out' }}>
            <h1 style={{ margin: '0', padding: '20px 0' }}>
              Login
            </h1>
            <form onSubmit={this.handleSubmit} style={{ width: '200px', margin: 'auto' }}>
              <div style={{ paddingTop: '5px' }}>
                <TextField
                  style={{
                    width: '100%',
                    boxSizing: 'border-box'
                  }}
                  ref={ref => { this._emailRef = ref; }}
                  floatingLabelText="Email"
                />
              </div>
              <div style={{ paddingTop: '5px' }}>
                <TextField
                  style={{
                    width: '100%',
                    boxSizing: 'border-box'
                  }}
                  ref={ref => { this._passwordRef = ref; }}
                  type="password"
                  floatingLabelText="Password"
                />
              </div>
              <div style={{ paddingTop: '10px', height: '1em', color: '#383838', fontWeight: 400 }}>{hint && `Hint: ${hint}`}</div>
              <div style={{ paddingTop: '5px' }}>
                <RaisedButton
                  type="submit"
                  style={{
                    width: '100%'
                  }}
                  label="Login"
                  primary
                />
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}
