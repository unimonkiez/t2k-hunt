import React, { Component, PropTypes } from 'react';
import geolib from 'geolib';
import Marker from './marker';

export default class Me extends Component {
  static propTypes = {
    // bearing: PropTypes.number,
    coordinate: PropTypes.shape({
      latitude: PropTypes.number,
      longitude: PropTypes.number
    }).isRequired
  };
  static defaultProps = {
    bearing: 0
  };
  static getImage({
    bearing,
    frame,
    moving
  }) {
    /* eslint-disable global-require, no-undef, import/no-unresolved */
    if (moving) {
      if (bearing > 45 && bearing <= 135) {
        switch (frame) {
          case 0:
          default:
            return require('../src/image/me/walk_right_01.png');
          case 1:
            return require('../src/image/me/walk_right_02.png');
          case 2:
            return require('../src/image/me/walk_right_03.png');
          case 3:
            return require('../src/image/me/walk_right_04.png');
          case 4:
            return require('../src/image/me/walk_right_05.png');
          case 5:
            return require('../src/image/me/walk_right_06.png');
          case 6:
            return require('../src/image/me/walk_right_07.png');
          case 7:
            return require('../src/image/me/walk_right_08.png');
        }
      } else if (bearing > 135 && bearing <= 225) {
        switch (frame) {
          case 0:
          default:
            return require('../src/image/me/walk_down_01.png');
          case 1:
            return require('../src/image/me/walk_down_02.png');
          case 2:
            return require('../src/image/me/walk_down_03.png');
          case 3:
            return require('../src/image/me/walk_down_04.png');
          case 4:
            return require('../src/image/me/walk_down_05.png');
          case 5:
            return require('../src/image/me/walk_down_06.png');
          case 6:
            return require('../src/image/me/walk_down_07.png');
          case 7:
            return require('../src/image/me/walk_down_08.png');
        }
      } else if (bearing > 225 && bearing <= 315) {
        switch (frame) {
          case 0:
          default:
            return require('../src/image/me/walk_left_01.png');
          case 1:
            return require('../src/image/me/walk_left_02.png');
          case 2:
            return require('../src/image/me/walk_left_03.png');
          case 3:
            return require('../src/image/me/walk_left_04.png');
          case 4:
            return require('../src/image/me/walk_left_05.png');
          case 5:
            return require('../src/image/me/walk_left_06.png');
          case 6:
            return require('../src/image/me/walk_left_07.png');
          case 7:
            return require('../src/image/me/walk_left_08.png');
        }
      } else {
        switch (frame) {
          case 0:
          default:
            return require('../src/image/me/walk_forward_01.png');
          case 1:
            return require('../src/image/me/walk_forward_02.png');
          case 2:
            return require('../src/image/me/walk_forward_03.png');
          case 3:
            return require('../src/image/me/walk_forward_04.png');
          case 4:
            return require('../src/image/me/walk_forward_05.png');
          case 5:
            return require('../src/image/me/walk_forward_06.png');
          case 6:
            return require('../src/image/me/walk_forward_07.png');
          case 7:
            return require('../src/image/me/walk_forward_08.png');
        }
      }
    } else {
      // eslint-disable-next-line no-lonely-if
      if (bearing > 45 && bearing <= 135) {
        return require('../src/image/me/walk_right_00.png');
      } else if (bearing > 135 && bearing <= 225) {
        return require('../src/image/me/walk_down_00.png');
      } else if (bearing > 225 && bearing <= 315) {
        return require('../src/image/me/walk_left_00.png');
      } else {
        return require('../src/image/me/walk_forward_00.png');
      }
    }
    /* eslint-enable global-require, no-undef, import/no-unresolved */
  }
  componentWillMount() {
    this.updateImage();
    this._interval = setInterval(() => this.updateImage(), 100);
  }
  componentWillUnmount() {
    clearInterval(this._interval);
  }
  updateImage() {
    const { coordinate } = this.props;
    const prevCoordinate = this._prevCoordinate !== undefined ? this._prevCoordinate : coordinate;
    const prevBearing = this._prevBearing !== undefined ? this._prevBearing : 0;
    const prevFrame = this._frame !== undefined ? this._frame : 0;
    let moving;
    let bearing;
    const frame = (prevFrame + 1) % 8;
    if (coordinate === prevCoordinate) {
      moving = false;
      bearing = prevBearing;
    } else {
      moving = true;
      bearing = geolib.getBearing({ ...prevCoordinate }, { ...coordinate });
    }
    const image = Me.getImage({
      bearing,
      frame,
      moving
    });
    this.setState({
      image
    });
    this._prevCoordinate = coordinate;
    this._prevBearing = bearing;
    this._frame = frame;
  }
  updatePreviousCoordinate({ coordinate }) {
    this._prevCoordinate = coordinate;
  }
  render() {
    const { coordinate } = this.props;
    const { image } = this.state;
    return (
      <Marker
        coordinate={coordinate}
        title="Yuval Saraf"
        image={image}
      />
    );
  }
}
