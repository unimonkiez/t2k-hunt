import React, { Component, PropTypes } from 'react';
import Marker from './marker';

export default class Me extends Component {
  static propTypes = {
    tgId: PropTypes.string.isRequired,
    tgVersion: PropTypes.string.isRequired,
    taskId: PropTypes.string.isRequired,
    answered: PropTypes.bool.isRequired,
    coordinate: PropTypes.shape({
      latitude: PropTypes.number,
      longitude: PropTypes.number
    }).isRequired,
    openTask: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);
    this.state = {
      isOpen: props.answered
    };
  }

  openTask() {
    this.setState({
      isOpen: true
    }, () => {
      setTimeout(() => {
        const { tgId, tgVersion, taskId, openTask } = this.props;
        openTask({ tgId, tgVersion, taskId });
      }, 100);
    });
  }

  render() {
    const { coordinate } = this.props;
    const { isOpen } = this.state;
    /* eslint-disable global-require, no-undef, import/no-unresolved, react/jsx-no-bind, import/no-dynamic-require */
    const openTask = this.openTask.bind(this);
    return (
      <Marker
        coordinate={coordinate}
        image={require(isOpen ? '../src/image/chest_open.png' : '../src/image/chest_closed.png')}
        onPress={openTask}
      />
    );
    /* eslint-enable global-require, no-undef, import/no-unresolved, react/jsx-no-bind, import/no-dynamic-require */
  }
}
