import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Text, Image } from 'react-native';
import { connect } from 'react-redux';

const leaderboardStyle = {
  row: {
    padding: 2,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  image: {
    width: 20,
    height: 20
  },
  text: {
    width: 20,
    color: 'white',
    fontFamily: 'monoosd'
  }
};

@connect(
  state => ({
    leaderScore: state.leaderboard
  })
)
class Leaderboard extends Component {
  static propTypes = {
    leaderScore: PropTypes.number.isRequired
  };
  render() {
    const { leaderScore } = this.props;
    return (
      <View>
        <View style={{ backgroundColor: '#1074b4', padding: 10, borderWidth: 1, borderColor: '#00116b', borderRadius: 5 }}>
          <View style={leaderboardStyle.row}>
            <Text style={leaderboardStyle.text}>1.</Text>
            <Image
              style={leaderboardStyle.image}
              source={{ uri: 'https://avatars1.githubusercontent.com/u/8074452?v=3&s=460' }}
            />
            <Text style={{ ...leaderboardStyle.text, paddingLeft: 3 }}>{leaderScore}</Text>
          </View>
          <View style={leaderboardStyle.row}>
            <Text style={leaderboardStyle.text}>2.</Text>
            <Image
              style={leaderboardStyle.image}
              source={{ uri: 'https://avatars0.githubusercontent.com/u/17742696?v=3&s=460' }}
            />
            <Text style={{ ...leaderboardStyle.text, paddingLeft: 3 }}>0</Text>
          </View>
          <View style={leaderboardStyle.row}>
            <Text style={leaderboardStyle.text}>3.</Text>
            <Image
              style={leaderboardStyle.image}
              source={{ uri: 'https://avatars2.githubusercontent.com/u/6780529?v=3&s=460' }}
            />
            <Text style={{ ...leaderboardStyle.text, paddingLeft: 3 }}>0</Text>
          </View>
        </View>
      </View>
    );
  }
}
export default Leaderboard;
