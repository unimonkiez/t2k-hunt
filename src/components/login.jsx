import React from 'react';
import PropTypes from 'prop-types';
import { View, Button } from 'react-native';

const LoginComponent = ({ login }) => (
  <View>
    <Button title={'Login'} onPress={() => login('omer', '12345')} />
  </View>
);
export default LoginComponent;
LoginComponent.propTypes = {
  login: PropTypes.func.isRequired
};
