import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Text } from 'react-native';

const style = {
  view: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  text: {
    fontSize: 50,
    color: '#1074b4',
    fontFamily: 'monoosd'
  }
};

class EarnedScore extends Component {
  static propTypes = {
    score: PropTypes.number.isRequired
  };
  render() {
    const { score } = this.props;
    return (
      <View style={style.view}>
        <Text style={style.text}>+{score}</Text>
      </View>
    );
  }
}
export default EarnedScore;
