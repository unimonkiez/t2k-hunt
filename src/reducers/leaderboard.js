import { ADD_SCORE } from 'src/constants/leaderboard';

const initialState = 0;

export default function content(state = initialState, action) {
  const { type, data } = action;
  switch (type) {
    case ADD_SCORE:
      return state + data.score;
    default:
      return state;
  }
}
