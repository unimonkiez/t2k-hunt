const initialState = [
  '32.08623790351859, 34.80449438095093, a59807d9-2419-436d-ae05-c112a32cfff3, 3, 51122abd-960c-836e-6753-8cce9291903f',
  '32.08574704212636, 34.80270266532898, a59807d9-2419-436d-ae05-c112a32cfff3, 3, 1319e99c-3eb5-82db-0c2b-aed3ef0d1aa9',
  '32.086992370517834, 34.80411350727081, a59807d9-2419-436d-ae05-c112a32cfff3, 3, 77295b6f-521b-984a-350e-711c437e6248',
  '32.08588793779577, 34.80212867259979, a59807d9-2419-436d-ae05-c112a32cfff3, 3, f66bbef0-4543-4b13-18d9-186e755ebae5'
].map(str => {
  const taskDetails = str.split(', ');
  return {
    latitude: Number(taskDetails[0]),
    longitude: Number(taskDetails[1]),
    tgId: taskDetails[2],
    tgVersion: taskDetails[3],
    taskId: taskDetails[4]
  };
});

export default function content(state = initialState, action) {
  switch (action.type) {
    default:
      return state;
  }
}
