import { combineReducers } from 'redux';
import todos from './todos';
import credentials from './credentials';
import walk from './walk';
import content from './content';
import leaderboard from './leaderboard';

export default combineReducers({
  todos,
  credentials,
  walk,
  content,
  leaderboard
});
