import * as types from 'src/constants/leaderboard';

export const addScore = data => {
  return {
    type: types.ADD_SCORE,
    data
  };
};

export const Y = 7;
