import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Main from './main';
// import Login from './login';

@connect(
  state => ({
    authenticated: state.credentials.authenticated
  })
)
export default class AppRoute extends Component {
  static propTypes = {
    authenticated: PropTypes.bool.isRequired
  }
  render() {
    const { authenticated } = this.props;
    if (authenticated) {
      return (
        <Main />
      );
    } else {
      return (
        <Main />
      );
    }
  }
}
