import React, { Component } from 'react';
import Provider from 'src/provider/index';
import AppRoute from './app-route';

export default class App extends Component {
  render() {
    return (
      <Provider>
        <AppRoute />
      </Provider>
    );
  }
}
