import React, { Component, PropTypes } from 'react';
import Leaderboard from 'src/components/leaderboard';
import EarnedScore from 'src/components/earned-score';
import Map from 'src/components/map';
import Me from 'src/components/me';
import Task from 'src/components/task';
import PointsFromTo from 'src/common/points-from-to';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import geolib from 'geolib';
import { View, StyleSheet, TouchableHighlight, WebView, Image } from 'react-native';
import { addScore } from 'src/actions/leaderboard';

const styles = StyleSheet.create({
  big: {
    flexDirection: 'column',
    height: '100%'
  },
  button: {
    position: 'absolute',
    right: 10,
    bottom: 10
  },
  buttonImage: {
    width: 131,
    height: 57
  },
  taskViewer: {
    position: 'absolute',
    right: 20,
    bottom: 20,
    top: 20,
    left: 20,
    backgroundColor: 'white'
  },
  webView: {
    flex: 1,
    backgroundColor: 'white'
  }
});

@connect(
  state => ({
    walk: state.walk,
    content: state.content
  }),
  dispatch => ({
    addScore: bindActionCreators(addScore, dispatch)
  })
)
export default class Main extends Component {
  static propTypes = {
    walk: PropTypes.arrayOf(PropTypes.shape({
      latitude: PropTypes.number,
      longitude: PropTypes.number
    })).isRequired,
    content: PropTypes.arrayOf(PropTypes.shape({
      latitude: PropTypes.number,
      longitude: PropTypes.number,
      tgId: PropTypes.string,
      tgVersion: PropTypes.string,
      taskId: PropTypes.string
    })).isRequired,
    addScore: PropTypes.func.isRequired
  };
  componentWillMount() {
    const { walk } = this.props;
    this._overallDistance = walk.reduce((sum, spot, i) => {
      const nextSpot = walk[i + 1];
      if (nextSpot) {
        return sum + geolib.getDistance(spot, nextSpot);
      } else {
        return sum;
      }
    }, 0); // meters
    this._walkSpeed = 10; // seconds
    this._moveFromPointEvery = 15; // ms
    this._currentStepIndex = 0;
    this._shouldStop = false;
    this._answeredTasks = [];
    this.state = {
      me: walk[0],
      fellow: true,
      earnedScore: undefined
    };
    this.walkToNext();
  }
  componentDidMount() {

  }
  onMessage(event) {
    const msg = event.nativeEvent.data;
    console.log(msg);
    if (msg === 'correct' || msg === 'wrong') {
      this._answeredTasks.push(this.state.currentTask.taskId);
      setTimeout(() => {
        this.setState({
          currentTask: null
        }, () => {
          const earnedScore = msg === 'correct' ? 10 : 5;
          this.props.addScore({ score: earnedScore });
          this.setState({
            earnedScore
          }, () => {
            setTimeout(() => {
              this.setState({ earnedScore: undefined });
            }, 3000);
          });
        });
      }, 500);
    }
  }
  walkToNext() {
    if (!this._walking) {
      const { walk } = this.props;
      const overallDistance = this._overallDistance;
      const walkSpeed = this._walkSpeed;
      const moveFromPointEvery = this._moveFromPointEvery;
      const currentStepIndex = this._currentStepIndex;
      const nextStepIndex = (currentStepIndex + 1) % (walk.length - 1);
      const spot = walk[currentStepIndex];
      const nextSpot = walk[nextStepIndex];
      const distance = geolib.getDistance(spot, nextSpot);
      const currWalkSpeed = (distance / overallDistance) * walkSpeed; // seconds
      const points = PointsFromTo({
        from: spot,
        to: nextSpot,
        splitBy: Math.ceil(currWalkSpeed * (1000 / moveFromPointEvery))
      });
      this._walking = true;
      const doPoint = () => {
        if (this._shouldStop) {
          setTimeout(() => {
            doPoint();
          }, moveFromPointEvery);
        } else {
          if (points.length > 0) {
            this.updateShouldStop(points[0]);
            const point = points.shift();
            setTimeout(() => {
              this.setState({
                me: point
              }, () => {
                doPoint();
              });
            }, moveFromPointEvery);
          } else {
            this._walking = false;
            this._currentStepIndex = nextStepIndex;
            this.walkToNext();
          }
        }
      };
      doPoint();
    }
  }
  updateShouldStop(currPoint) {
    const { content } = this.props;
    this._shouldStop = content.filter(task => (geolib.getDistance(task, currPoint) < 30))
        .filter(task => (this._answeredTasks.indexOf(task.taskId) === -1)).length > 0;
  }
  handleRegionChange(region) {
    if (this.timesFired >= 2) {
      console.log(region);
      this.setState({
        fellow: false,
        region
      });
    } else {
      this.timesFired = this.timesFired ? this.timesFired : 0;
      this.timesFired += 1;
    }
  }
  handleFellowPress() {
    this._shouldStop = false;
    this.setState({
      fellow: true
    });
  }
  openTask(currentTask) {
    if (this._answeredTasks.indexOf(currentTask.taskId) === -1) {
      this.setState({
        currentTask
      });
    }
  }
  /* eslint-disable react/jsx-no-bind, global-require, no-undef, import/no-unresolved, import/no-dynamic-require */
  render() {
    const { me, fellow, region, currentTask, earnedScore } = this.state;
    const { content } = this.props;
    const tasks = content.filter(task => (geolib.getDistance(task, me) < 50));
    const mapRegion = fellow ? ({
      ...me,
      latitudeDelta: 0.005,
      longitudeDelta: 0.0015
    }) : region;

    return (
      <View style={styles.big}>
        <Map
          region={mapRegion}
          onRegionChange={val => { this.handleRegionChange(val); }}
        >
          <Me
            moving
            coordinate={me}
          />
          {tasks.map(task => {
            return (<Task
              coordinate={{ latitude: task.latitude, longitude: task.longitude }}
              key={`task-${task.taskId}`}
              openTask={this.openTask.bind(this)}
              answered={(this._answeredTasks.indexOf(task.taskId) !== -1)}
              {...task}
            />);
          })}

        </Map>
        <TouchableHighlight style={styles.button} onPress={() => this.handleFellowPress()}>
          <Image
            style={styles.buttonImage}
            source={require(fellow ? '../src/image/follow/btn_follow_active.png' : '../src/image/follow/btn_follow_inactive.png')}
          />
        </TouchableHighlight>
        <View style={{ position: 'absolute', top: 5, right: 5 }}><Leaderboard /></View>
        {currentTask && <View style={styles.taskViewer}><WebView
          style={styles.webView}
          source={{ uri: `http://echo-hunt.herokuapp.com/tg/index.html?tg=${currentTask.tgId}&ver=${currentTask.tgVersion}&taskId=${currentTask.taskId}` }}
          onMessage={this.onMessage.bind(this)}
        /></View>}
        {
          earnedScore &&
          <View
            style={{
              position: 'absolute',
              top: 0,
              left: 0,
              right: 0,
              bottom: 0,
              justifyContent: 'center',
              alignItems: 'center' }}
          >
            <EarnedScore score={earnedScore} />
          </View>
        }

      </View>
    );
    /* eslint-enable react/jsx-no-bind, global-require, no-undef, import/no-unresolved, import/no-dynamic-require */
  }

}
