import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as CredentialsActions from 'src/actions/credentials-actions';
import Login from 'src/components/login';
// import firebase from 'src/firebase/index';

@connect(
  state => ({
    authenticated: state.credentials.authenticated,
    checkingToken: state.credentials.checkingToken,
    loggingIn: state.credentials.loggingIn,
    hint: state.credentials.hint
  }),
  dispatch => ({
    credentialsActions: bindActionCreators(CredentialsActions, dispatch)
  })
)
export default class LoginContainer extends Component {
  static propTypes = {
    credentialsActions: PropTypes.shape({
      clearCredentials: PropTypes.func.isRequired,
      checkCredentials: PropTypes.func.isRequired,
      checkCredentialsSucess: PropTypes.func.isRequired,
      checkCredentialsFailure: PropTypes.func.isRequired,
      addCredentials: PropTypes.func.isRequired,
      addCredentialsSucess: PropTypes.func.isRequired,
      addCredentialsFailure: PropTypes.func.isRequired
    }).isRequired,
    checkingToken: PropTypes.bool.isRequired,
    loggingIn: PropTypes.bool.isRequired,
    hint: PropTypes.bool.isRequired
  };
  login = this.login.bind(this);
  login(email, password) {
    console.log(email, password);
    this.props.credentialsActions.addCredentials();
    setTimeout(() => this.props.credentialsActions.addCredentialsSucess(), 1000);
  }
  render() {
    return (
      <Login
        login={this.login}
        checkingToken={this.props.checkingToken}
        loggingIn={this.props.loggingIn}
        hint={this.props.hint}
      />
    );
  }
}
