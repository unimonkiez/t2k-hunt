import React from 'react';
import ReactDOM from 'react-dom';
import App from 'src/containers/app';
import AppRoute from 'src/containers/app-route';
import injectTapEventPlugin from 'react-tap-event-plugin';

injectTapEventPlugin(); // Needed for onTouchTap

ReactDOM.render(
  <App>
    <AppRoute />
  </App>,
  document.getElementById('app')
);
