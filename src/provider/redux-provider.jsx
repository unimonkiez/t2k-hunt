import { Provider } from 'react-redux';
import React, { Component, PropTypes } from 'react';
import configureStore from 'src/store/configure-store';

const store = configureStore();

export default class WebProvider extends Component {
  static propTypes = {
    children: PropTypes.node.isRequired
  }
  render() {
    return (
      <Provider store={store}>
        {this.props.children}
      </Provider>
    );
  }
}
