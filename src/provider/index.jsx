import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ReduxProvider from './redux-provider';

export default class WebProvider extends Component {
  static propTypes = {
    children: PropTypes.node.isRequired
  };
  render() {
    return (
      <ReduxProvider>
        {this.props.children}
      </ReduxProvider>
    );
  }
}
