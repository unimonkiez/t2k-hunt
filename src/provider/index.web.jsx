import React, { Component, PropTypes } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import DevTools from 'src/util/dev-tools';
import ReduxProvider from './redux-provider';

export default class WebProvider extends Component {
  static propTypes = {
    children: PropTypes.node.isRequired
  };
  render() {
    return (
      <ReduxProvider>
        <MuiThemeProvider>
          <div>
            {this.props.children}
            {__DEVTOOLS__ && <DevTools />}
          </div>
        </MuiThemeProvider>
      </ReduxProvider>
    );
  }
}
