export default ({
  from,
  to,
  splitBy
}) => {
  const deltaLat = (to.latitude - from.latitude) / splitBy;
  const deltaLon = (to.longitude - from.longitude) / splitBy;

  return Array.from({ length: splitBy }).map((_, i) => ({
    latitude: from.latitude + (deltaLat * (i + 1)),
    longitude: from.longitude + (deltaLon * (i + 1))
  }));
};
