const getWebpackConfig = require('./get-webpack-config');
const webpack = require('webpack');
const webpackMiddleware = require('webpack-dev-middleware');
const webpackHotMiddleware = require('webpack-hot-middleware');
const getApp = require('../web/get-app');

const app = getApp();
const port = 8080;

const start = () => {
  app.listen(port, err => {
    if (err) {
      throw err;
    }
    console.log(`Listening at localhost:${port}`);
  });
};

const webpackConfig = getWebpackConfig({
  isMiddleware: true,
  port
});

const webpackCompiler = webpack(webpackConfig);
const webpackDevMiddlewareInstance = webpackMiddleware(webpackCompiler,
  {
    publicPath: '',
    noInfo: false,
    quiet: false
  }
);
const webpackHotMiddlewareInstance = webpackHotMiddleware(webpackCompiler, {
  log: console.log,
  heartbeat: 3 * 1000
});

app.use(webpackDevMiddlewareInstance);
app.use(webpackHotMiddlewareInstance);
webpackDevMiddlewareInstance.waitUntilValid(start);
