const BUILD_TYPE = require('./build-type');
const webpack = require('webpack');
const path = require('path');
const nodeExternals = require('webpack-node-externals');

// Plugins
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const extensions = ['.js', '.jsx'];
const rootPath = path.join(__dirname, '..');

/**
 * Getter for webpack config for all the different kind of builds there is in this repo
 * @param  {Object} options        Passed from building, starting and testing the application
 * @param  {Object} privateOptions Only passed from `getServerString`, used to add some private options relevent only to this file
 * @return {Object}                Webpack config object
 */
module.exports = ({
  type = BUILD_TYPE.web,
  coveragePaths = [],
  isMiddleware = false,
  isTest = false,
  isCoverage = false,
  bail = false
} = {}) => {
  const isProd = !isMiddleware;
  let deviceExtensions;
  let filesPrefix;
  let outputPath;
  switch (type) {
    default:
    case BUILD_TYPE.web:
      deviceExtensions = extensions.map(extension => `.web${extension}`);
      filesPrefix = `app${isProd ? '.min' : ''}`;
      outputPath = path.join(rootPath, 'web', 'dist');
      break;
    case BUILD_TYPE.ios:
      deviceExtensions = extensions.map(extension => `.ios${extension}`);
      filesPrefix = 'index.ios';
      outputPath = path.join(rootPath, 'dist');
      break;
    case BUILD_TYPE.android:
      deviceExtensions = extensions.map(extension => `.android${extension}`);
      filesPrefix = 'index.android';
      outputPath = path.join(rootPath, 'dist');
      break;
  }

  return ({
    bail,
    target: isTest ? 'node' : undefined,
    devtool: (isTest || type !== BUILD_TYPE.web) ? 'inline-source-map' : 'source-map',
    entry: isTest ? undefined : {
      [filesPrefix]: []
      .concat(isMiddleware ? ['webpack-hot-middleware/client'] : [])
      .concat(path.join(rootPath, 'src', 'index'))
    },
    output: isTest ? undefined : {
      path: outputPath,
      filename: '[name].js',
      libraryTarget: 'umd'
    },
    plugins: [
      new ExtractTextPlugin({
        filename: '[name].css',
        disable: isMiddleware,
        allChunks: true
      }),
      new webpack.DefinePlugin({
        __PROD__: JSON.stringify(isProd),
        __DEV__: JSON.stringify(!isProd),
        __DEVSERVER__: JSON.stringify(isMiddleware),
        __DEVTOOLS__: JSON.stringify(isMiddleware),
        __IOS__: JSON.stringify(type === BUILD_TYPE.ios),
        __ANDROID__: JSON.stringify(type === BUILD_TYPE.android),
        __WEB__: JSON.stringify(type === BUILD_TYPE.web),
        'process.env': {
          NODE_ENV: JSON.stringify(isProd ? 'production' : 'development')
        }
      })
    ]
    .concat(type === BUILD_TYPE.web ? [
      new HtmlWebpackPlugin({
        minify: {},
        template: path.join(rootPath, 'src', 'index.ejs'),
        inject: 'body'
      })
    ] : [])
    .concat((isTest || type !== BUILD_TYPE.web) ? [] : new webpack.ProvidePlugin({
      fetch: 'imports?this=>global!exports?global.fetch!whatwg-fetch'
    }))
    .concat(isMiddleware ? [
      new webpack.HotModuleReplacementPlugin()
    ] : [])
    .concat(isProd ? [
      new webpack.optimize.UglifyJsPlugin({
        exclude: BUILD_TYPE.web ? [] : ['src'],
        output: {
          comments: false
        }
      })
    ] : []),
    module: {
      rules: []
      // Very important that "instrumention" will be before other loaders in array, otherwise fail (what?)
      .concat(isCoverage ? [
        {
          test: /\.(js|jsx)$/,
          include: coveragePaths,
          loader: {
            loader: 'istanbul-instrumenter-loader'
          }
        }
      ] : [])
      .concat([
        {
          test: /\.js$/,
          exclude: /node_modules/,
          use: [
            {
              loader: 'babel-loader',
              options: {
                presets: ['es2015', 'stage-2'],
                plugins: ['transform-runtime', 'transform-decorators-legacy']
              }
            }
          ]
        },
        {
          test: /\.jsx$/,
          exclude: /node_modules/,
          use: [
            {
              loader: 'babel-loader',
              options: {
                presets: ['es2015', 'stage-2', 'react'].concat(isMiddleware ? ['react-hmre'] : []),
                plugins: ['transform-runtime', 'transform-decorators-legacy']
              }
            }
          ]
        },
        {
          test: /\.css$/,
          use: ExtractTextPlugin.extract({
            use: [
              {
                loader: 'css-loader'
              }
            ],
            fallback: 'style-loader'
          })
        }, {
          test: /\.scss$/,
          use: ExtractTextPlugin.extract({
            use: [
              {
                loader: 'css-loader'
              },
              {
                loader: 'resolve-url-loader',
                options: {
                  fail: true
                }
              },
              {
                loader: 'sass-loader'
              }
            ],
            fallback: 'style-loader'
          })
        }, {
          test: /\.woff(2)?$/,
          use: [
            {
              loader: 'url-loader',
              options: {
                limit: 10000,
                name: './font/[hash].[ext]',
                mimetype: 'application/font-woff'
              }
            }
          ]
        }, {
          test: /\.(ttf|eot|svg)$/,
          use: [
            {
              loader: 'url-loader',
              options: {
                limit: 10000,
                name: './font/[hash].[ext]'
              }
            }
          ]
        }, {
          test: /\.(gif|png)$/,
          use: [
            {
              loader: 'url-loader',
              options: {
                limit: 10000,
                name: './asset/[hash].[ext]'
              }
            }
          ]
        }
      ])
    },
    resolve: {
      extensions: []
      .concat(deviceExtensions)
      .concat(extensions),
      modules: [
        rootPath,
        path.join(rootPath, 'node_modules')
      ]
    },
    externals: []
    .concat(([BUILD_TYPE.ios, BUILD_TYPE.android].indexOf(type) !== -1) ? [
      'react',
      'react-native',
      'react-native-maps'
    ] : [])
    .concat(([BUILD_TYPE.ios, BUILD_TYPE.android].indexOf(type) !== -1) ? (context, request, callback) => {
      if (request.match(/\.(png|jpg)$/g)) {
        // mark this module as external
        // https://webpack.github.io/docs/configuration.html#externals
        return callback(null, `commonjs ${request}`);
      }
      callback();
      return undefined;
    } : [])
    .concat(isTest ? nodeExternals() : [])
  });
};
