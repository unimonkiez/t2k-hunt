const webpack = require('webpack');
const getWebpackConfig = require('./get-webpack-config');

const args = process.argv.slice(2);
const isWatching = args.indexOf('-w') !== -1;
const typeIndex = args.indexOf('--type');

const typeValue = args[typeIndex + 1];
const buildType = Number(typeValue);

const webpackConfig = getWebpackConfig({
  type: buildType,
  bail: !isWatching
});

const cb = (err, stats) => {
  if (err) {
    console.warn(err);
    if (!isWatching) {
      process.exit(1);
    }
  } else {
    console.log('[webpack log]', stats.toString());
  }
};
if (isWatching) {
  webpack(webpackConfig).watch({}, cb);
} else {
  webpack(webpackConfig, cb);
}
