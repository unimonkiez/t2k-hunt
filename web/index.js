const getApp = require('./get-app');

const app = getApp();

const port = process.env.PORT || 8080;

const start = () => {
  app.listen(port, err => {
    if (err) {
      throw err;
    }
    console.log(`Listening at localhost:${port}`);
  });
};

start();
