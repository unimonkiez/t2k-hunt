const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const session = require('express-session');
const apiRoute = require('./api');

module.exports = () => {
  const app = express();

  app.use(bodyParser.json()); // support json encoded bodies
  app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies
  app.use(session({ secret: 'shhhh!!', saveUninitialized: true, resave: false, cookie: { maxAge: 3600000 } })); // Hour


  const staticPath = path.join(__dirname, 'dist');
  const tgViewerPath = path.join(__dirname, 'taskGroupViewer');

  app.use(express.static(staticPath));
  app.use('/tg', express.static(tgViewerPath));
  app.use('/api', apiRoute);

  return app;
};
