const { Router } = require('express');

const router = new Router();

router.get('/config', (req, res) => {
  res.json('success');
});

module.exports = router;
