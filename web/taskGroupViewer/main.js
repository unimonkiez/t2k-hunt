var interactionApi;
var taskGroupPlayer;
var userId = '5aba03db-8a2c-434f-8684-0b563d6ad9dc';
var serviceUrl = 'https://api.devqa.timetoknow.com';

function login({ serviceUrl, email, password, site }) {
  return fetch(`${serviceUrl}/Interactions/login?username=${email}&password=${password}&site=${site}`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    }
  })
  .then(response => response.json())
  .then(data => data.access_token)
  .catch(err => Promise.reject(err));
}
login({
  serviceUrl,
  site: 'noam',
  email: 'instructor_1',
  password: '123456'
}).then(token => {
  var getURLParam = function (name) {
    name = name.replace(/[\[]/, '\\\[').replace(/[\]]/, '\\\]');
    var regexS = '[\\?&]' + name + '=([^&#]*)';
    var regex = new RegExp(regexS);
    var results = regex.exec(window.location.href);
    if (results !== null) {
      return results[1];
    }
  };

  var loadInteractions = function () {
    var apiConfig = {
      serviceUrl: serviceUrl,
      authenticationToken: token
    };

    window.t2kinteraction.getApi(apiConfig, function (error, api) {
      interactionApi = api;
      loadTaskGroup();
    });
  };

  var loadTaskGroup = function () {
    if (interactionApi) {
      interactionApi.playTaskGroup(
        {
          taskGroupIdentity: {
            taskGroupId: getURLParam('tg'),
            taskGroupVersion: Number(getURLParam('ver'))
          },
          userStateOptions: {
            userId: userId,
            isPersistent: false
          },
          getEventsContext: function (doneCallback) {
            doneCallback({
              providerId: '',
              data: {}
            });
          }
        },
        getPlayTaskGroupCallback()
      );
    }
  };

  var getPlayTaskGroupCallback = function () {
    return function (error, player) {
      taskGroupPlayer = player;
      loadTask();
    }
  };

  var loadTask = function () {
    var viewOptions = {
      isReadOnly: false,
      showSystemAnswers: false,
      singleTaskMode: true,
      hidePoweredBy: true,
      viewTaskId: getURLParam('taskId'),
      // taskGroupPlayerViewType: this.props.viewType,
      showTaskGroupReport: false,
      ignoreEvents: true,
      isTaskGroupVisible: true,
      viewMode: window.t2kinteraction.TASK_GROUP_VIEW_MODE.QUIZ,
      taskStartingIndex: 1
    };
    var domElement = document.getElementById('root');
    taskGroupPlayer.mount(
      {
        domElement,
        defaultViewOptions: viewOptions
      },
      checkAnswer
    );
  };

  var checkAnswer = function () {
    var isWrong = !!document.querySelectorAll('.t2k-button.progress-btn.wrong').length;
    var isCorrect = !!document.querySelectorAll('.t2k-button.progress-btn.correct').length;
    if (isWrong || isCorrect) {
      var msg = isWrong ? 'wrong' : 'correct';
      window.postMessage(msg, '*');
    } else {
      setTimeout(checkAnswer, 200);
    }
  };

  loadInteractions();
});
